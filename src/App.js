import React from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';

import './css/Main.css';

import spaceship from './images/spaceship.png';
import spaceship1 from './images/spaceship1.png';
import spaceship2 from './images/spaceship2.png';
import spaceship3 from './images/spaceship3.png';
import spaceship4 from './images/spaceship4.png';
import spaceship5 from './images/spaceship5.png';
import spaceship6 from './images/spaceship6.png';
import gold from './images/gold.png';
import distance from './images/distance.png';



class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      items: [],
      images: [spaceship, spaceship1, spaceship2, spaceship3, spaceship4, spaceship5, spaceship6],
      randomNumber: 0,
      starShips: [],
      gold: 0,
      distance: 0,
      player1: '',
      player2: '',
      vehicle1: '',
      vehicle2: '',
      vehicle1Mass: 0,
      vehicle2Mass: 0,
      vehicle1Speed: 0,
      vehicle2Speed: 0,
      // final conclusions
      timeNeededPlayer1: 0,
      timeNeededPlayer2: 0,
      tripsMadePlayer1: 0,
      tripsMadePlayer2: 0,
      scorePlayer1: 0,
      scorePlayer2: 0,
      winner: '',
      gameWelcome : true,
      firstLoad : true,

    }

    this.playGame = this.playGame.bind(this);
    this.startGame = this.startGame.bind(this);
    // this.playAgain = this.playAgain.bind(this);

  }

  startGame(){
    this.setState({
      gameWelcome : false,
    })
  }



  playAgain() {
    //Clean all the states
    this.setState({
      gold: 0,
      distance: 0,
      player1: '',
      player2: '',
      vehicle1: '',
      vehicle2: '',
      vehicle1Mass: 0,
      vehicle2Mass: 0,
      vehicle1Speed: 0,
      vehicle2Speed: 0,
      // final conclusions
      timeNeededPlayer1: 0,
      timeNeededPlayer2: 0,
      tripsMadePlayer1: 0,
      tripsMadePlayer2: 0,
      winner: '',

    })
    //After cleaning the state, we need to generate the players again

    let gameInfo = this.state.items.results; // Save Data inside the variable
    let randomFactor = 0; // Int a new var with 0, this will be our randomNumber
    let players = [] //String used to save the player's information

    //looping inside api's information and pushing the players to var players
    gameInfo.map(item => {
      if (item.name !== '') {
        randomFactor++; //Get the object length
        players.push(item.name);
      }
    });
    let randomN1 = (Math.floor(Math.random() * randomFactor)); //generate a new randomNumber according to the object length
    let randomN2 = (Math.floor(Math.random() * randomFactor));
    let player1 = players[randomN1]
    let player2 = players[randomN2]

    this.setState({
      player1: player1,
      player2: player2,
    })

    // Then we do the logic again with the spaceships
    let spaceShipsInfo = this.state.starShips.results;
    let randomFactorSpaceShips = 0;
    let spaceShips = []

    spaceShipsInfo.map(item => {
      if ((item.cargo_capacity !== 'unknown' && item.max_atmosphering_speed !== 'unknown') || (item.cargo_capacity !== 'unknown' && item.max_atmosphering_speed !== 'n/a')){
        randomFactorSpaceShips++;
        spaceShips.push({ "name": item.name, "speed": item.max_atmosphering_speed, "cargo": item.cargo_capacity })
      }
    });
    /*
    spaceShipsInfo.filter(item=> (item.cargospaceshipapacity !== 'unknown')&&((item.max_atmosphering_speed !== 'n/a')))
    .map(item=> {
      randomFactorSpaceShips++;
      spaceShips.push({ "name": item.name, "speed": item.max_atmosphering_speed, "cargo": item.cargo_capacity });
    })*/


    let randomN1_ = (Math.floor(Math.random() * randomFactorSpaceShips)); //generate a new randomNumber according to the object length
    let randomN2_ = (Math.floor(Math.random() * randomFactorSpaceShips));

    if (randomN1_ === randomN2_) {
       return randomN1_++; //This will avoid the random numbers to be equals
    }
    console.log(spaceShips);
    console.log("____________")
    let Gold = (Math.floor(Math.random() * (9999 - 100) + 100))
    let Distance = (Math.floor(Math.random() * (99999 - 1000) + 1000))
    this.setState({
      vehicle1: spaceShips[randomN1_].name,
      vehicle2: spaceShips[randomN2_].name,
      vehicle1Mass: (spaceShips[randomN1_].cargo),
      vehicle2Mass: spaceShips[randomN2_].cargo,
      vehicle1Speed: spaceShips[randomN1_].speed,
      vehicle2Speed: spaceShips[randomN2_].speed,
      randomNumber : Math.floor(Math.random() * 6),
      gold: Gold,
      distance: Distance,
    })
    //After generating all the data needed, we call "playAgain" to restart the game
    this.playGame()
  }

  playGame() {
    
    let tripsPlayer1 = Math.round((this.state.gold) / Number(this.state.vehicle1Mass));
    let tripsPlayer2 = Math.round((this.state.gold) / Number(this.state.vehicle2Mass));
    console.log(`Trips Player One: ${tripsPlayer1}, Player Two: ${tripsPlayer2}`)
    console.log(`Trips Player One typeOF: ${typeof tripsPlayer1}, Player Two typeOF: ${typeof tripsPlayer2}`)
    console.log(`State Vehicle 1 cargo ${typeof this.state.vehicle1Mass}`)
    console.log(`State Vehicle 2 cargo ${typeof this.state.vehicle2Mass}`)
    console.log(`State Gold ${this.state.gold}`)
    console.log(`State Gold typeOF ${typeof this.state.gold}`)
    console.log("____________")



    //Let's find the time in hours needed
    let timeNeededPlayer1 = Math.round(this.state.distance / Number(this.state.vehicle1Speed));
    let timeNeededPlayer2 = Math.round(this.state.distance / Number(this.state.vehicle2Speed));
    console.log(`Time needed Player One: ${timeNeededPlayer1}, Player Two: ${timeNeededPlayer2}`)
    console.log(`Time needed Player One typeOF: ${typeof timeNeededPlayer1}, Player Two typeOF: ${typeof timeNeededPlayer2}`)
    console.log(`State Vehicle 1 speed ${typeof this.state.vehicle1Speed}`)
    console.log(`State Vehicle 2 speed ${typeof this.state.vehicle2Speed}`)
    console.log(`State Distance ${this.state.distance }`)
    console.log(`State Distance typeOF ${typeof this.state.distance }`)
    console.log("____________")

    /*Let's find the trips needed to complete the mission
    let tripsPlayer1 = Math.round(parseInt(this.state.gold) / parseInt(this.state.vehicle1Mass));
    let tripsPlayer2 = Math.round(parseInt(this.state.gold) / parseInt(this.state.vehicle2Mass));

    //Let's find the time in hours needed
    let timeNeededPlayer1 = Math.round(parseInt(this.state.distance) / parseInt(this.state.vehicle1Speed));
    let timeNeededPlayer2 = Math.round(parseInt(this.state.distance) / parseInt(this.state.vehicle2Speed));*/

    //Let's find the time used to load and unload the ship in hours needed

    let totalTimeP1 = timeNeededPlayer1 + ((tripsPlayer1 === 0 ? tripsPlayer1 + 1 : tripsPlayer1) * 2);
    let totalTimeP2 = timeNeededPlayer2 + ((tripsPlayer2 === 0 ? tripsPlayer2 + 1 : tripsPlayer2) * 2);
    console.log(`Total Time Player 1 ${totalTimeP1}`)
    console.log(`Total Time Player 2 ${totalTimeP2}`)
    console.log("____________")

    //Trips needed
    if ((tripsPlayer1) > 1) {
      this.setState({
        tripsMadePlayer1: tripsPlayer1,
      })
    }
    if ((tripsPlayer2) > 1) {
      this.setState({
        tripsMadePlayer2: tripsPlayer2,
      })
    }
    if ((tripsPlayer1) < 1) {
      this.setState({
        tripsMadePlayer1: 1,
      })
    }
    if ((tripsPlayer2) < 1) {
      this.setState({
        tripsMadePlayer2: 1,
      })
    }
    //Time needed
    this.setState({
      timeNeededPlayer1: totalTimeP1,
      timeNeededPlayer2: totalTimeP2,
      firstLoad : false,
    })

    //Player who does the trips in less time, wins
    if (totalTimeP1 < totalTimeP2) {
      let scorePlayer1 = this.state.scorePlayer1;
      this.setState({
        scorePlayer1: scorePlayer1 + 1,
        winner: 'Player One Wins!!!',
      })
    }
    if (totalTimeP1 > totalTimeP2) {
      let scorePlayer2 = this.state.scorePlayer2;
      this.setState({
        scorePlayer2: scorePlayer2 + 1,
        winner: 'Player Two Wins!!!',
      })
      console.log(scorePlayer2);
    }
    if (totalTimeP1 === totalTimeP2) {
      this.setState({
        winner: 'Oops, nobody wins',
      })
    }

  }

  componentDidMount() {
    let itemPerson = Math.floor(Math.random() * 9) + 1 //Se necesita mejorar el fectch de la aplicaciónx
    fetch(`https://swapi.co/api/people/?page=${itemPerson}`)
      .then(res => res.json())
      .then(json => {
        let gameInfo = json.results; // Save the results inside the variable
        let randomFactor = 0; // Int a new var with 0 
        let players = []

        gameInfo.map(item => {
          if (item.name !== '') {
            randomFactor++; //Get the object length
            players.push(item.name);
          }
        });
        let randomN1 = (Math.floor(Math.random() * randomFactor)); //generate a new randomNumber according to the object length
        let randomN2 = (Math.floor(Math.random() * randomFactor));
        let player1 = players[randomN1]
        let player2 = players[randomN2]

        this.setState({
          isLoaded: true,
          items: json,
          player1: player1,
          player2: player2,
        })
      })
      //.catch(err => ([{ error: err }]));

    // Now, we can fetch the spaceship data

    let randomShipNumber = Math.floor(Math.random() * 3) + 1;
    fetch(`https://swapi.co/api/starships/?page=${randomShipNumber}`)
      .then(res => res.json())
      .then(json => {
        this.setState({
          starShips: json,
        })
        //Let's use the json results and treat the data
        let spaceShipsInfo = json.results;
        let randomFactorSpaceShips = 0; //Random number, so we can random the ships into state
        let spaceShips = []

        //Let's use only the ships with defined values
        spaceShipsInfo.map(item => {
          if((item.cargospaceshipapacity !== 'unkown') ||   (item.max_atmosphering_speed !== 'unkown')){
            if((item.max_atmosphering_speed !== 'n/a')){
            randomFactorSpaceShips++;
            spaceShips.push({ "name": item.name, "speed": item.max_atmosphering_speed, "cargo": item.cargo_capacity });}
          }
        })    

        let randomN1 = (Math.floor(Math.random() * randomFactorSpaceShips)); //generate a new randomNumber according to the object length
        let randomN2 = (Math.floor(Math.random() * randomFactorSpaceShips));
        if (randomN1 === randomN2) {
          randomN1++; //This will avoid the random numbers to be equals
        }
        
        //Generating the gold and distance
        let Gold = (Math.floor(Math.random() * (9999 - 100) + 100))
        let Distance = (Math.floor(Math.random() * (99999 - 1000) + 1000))

        this.setState({
          vehicle1: spaceShips[randomN1].name,
          vehicle2: spaceShips[randomN2].name,
          vehicle1Mass: spaceShips[randomN1].cargo,
          vehicle2Mass: spaceShips[randomN2].cargo,
          vehicle1Speed: spaceShips[randomN1].speed,
          vehicle2Speed: spaceShips[randomN2].speed,
          gold: Gold,
          distance: Distance,
        })
      })
      //.catch(err => ([{ error: err }]))
  }

  render() {
    if (this.state.gameWelcome === true) {
      return (
        <div className="app-game-introduction">
          <div className="app-game-introduction-text">
          <p>
          Welcome to the Star Wars Game, this is a simple game created with ReactJS, using JavaScript and CSS.
The logic of the Game is easy, the app connects to the Star Wars API, fetch the data and randomly choose two players and assign them two SpaceShips.
            
The Gold and the Distance is also generated randomly by the game. Then the JavaScript does the magic playing with States, If Conditions and Loops.
            
I had to make some adaptations for the game, since the API has some limitation. No more talk, go ahead and see the magic happens...

May the force be with you!
          </p>
          <button type="button" onClick={() => this.startGame()}>Start</button>
        </div>
        </div>
      )
    }

    if (this.state.isLoaded === true) {
      return (
        <Container>
          <Col md={12} className="app-game-container">
            <Row>
              <Col className="app-player-one" md={4}>
                <Col md={12}>
                  <h2>Player One</h2>
                  <h4>{this.state.player1}</h4>
                  <p className="app-vehicle-name">{this.state.vehicle1}</p>
                  <p>Max Speed :{this.state.vehicle1Speed} km/h</p>
                  <p>Cargo Capacity :{(this.state.vehicle1Mass * 1)} kgs</p>
                  <p>Number of trips: {this.state.tripsMadePlayer1} </p>
                  <p>Time consumed: {this.state.timeNeededPlayer2 > 24 ? Math.round((this.state.timeNeededPlayer1 / 24))+" days": (this.state.timeNeededPlayer1)+" hours"}</p>
                </Col>
                <Col>
                  <img className="app-spaceship-image" src={this.state.images[this.state.randomNumber]} alt="spaceship" />
                </Col>
                <Col className="app-spinning-planet"></Col>
              </Col>
              <Col className="app-game-display" md={4}>
                <Col>
                <Col md={12} className="app-game-display-score"><h4>Score</h4></Col>
                <span>{this.state.scorePlayer1}</span><span>VS</span><span>{this.state.scorePlayer2}</span>
                </Col>
                <Col className="app-game-winner" md={12}>
                  {this.state.winner}
                </Col>
                <Col md={12} className="app-game-display-resources">
                  <img src={gold} alt="gold"/>
                  <p style={{color: 'darkgoldenrod'}}>{this.state.gold}</p>
                  <img src={distance} alt="distance"/>
                  <p>{this.state.distance}</p>
                </Col>
                <Col>
                  {this.state.firstLoad === true ? <Button type="submit" onClick={() => this.playGame()}>Play Game!</Button> : <Button type="submit" onClick={() => this.playAgain()}>PlayAgain</Button>}
                </Col>
              </Col>
              <Col className="app-player-two" md={4}>
                <Col md={12}>
                <h2>Player Two</h2>
                <h4>{this.state.player2}</h4>
                <p className="app-vehicle-name">{this.state.vehicle2}</p>
                <p>Max Speed :{this.state.vehicle2Speed} km/h</p>
                <p>Cargo Capacity :{(this.state.vehicle2Mass * 1)} kgs</p>
                <p>Number of trips: {this.state.tripsMadePlayer2} </p>
                <p>Time consumed: {this.state.timeNeededPlayer2 > 24 ? Math.round((this.state.timeNeededPlayer2/ 24))+" days": (this.state.timeNeededPlayer2)+" hours"}</p>
                </Col><Col>
                <img className="app-spaceship-image" src={this.state.images[this.state.randomNumber + 1]} alt="spaceship" />
              </Col>
              </Col>
            </Row>
          </Col>
        </Container>
      );
    } else {
      return (
        <Container>
          <Col style={{ margin: 'auto', textAlign: 'center' }}>
            <p>Loading</p>
            <div className="app-loading"><i className="fas fa-spinner"></i></div>
          </Col>
        </Container>
      )
    }

  }
}

export default App;
